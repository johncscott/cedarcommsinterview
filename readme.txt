Feel free to contact me at andrew.walker@cedarcom.co.uk with any issues/clarifications

Requirements
Visual Studio (2019 Community edition is free)
.NET Framework 4.7.2 (picking 4.7.1 and 4.8 during the install of Visual studio worked fine with the test project)
Means to clone a Bitbucket source repo, branch, commit and push (git bash etc)

Steps

Perform a clone of the repo: git clone https://bitbucket.org/cedardigital/cedar-interview-2021.git
Create a branch off master for your work

[Web.config is already configured - Database should be accessible over the internet]

Umbraco username/password: monkler@monkler.com l3tm3inpls

Add an Article type to the build with Title and Content fields [Content is  WYSIWYG]
Create a view for it
Update the solution to output a JSON representation of the model. There is a placeholder for this in _Layout.cshtml. Note: Please ensure the properties are output in camelCase
Commit and push your branch
