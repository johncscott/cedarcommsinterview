﻿using Site.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Site.Controllers
{
    public interface IBaseController
    {
        IBaseItem Model { get; }
        string ViewPath { get; }

        void FillModel();
    }
}
