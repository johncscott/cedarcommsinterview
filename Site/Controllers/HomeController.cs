﻿using Site.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace Site.Controllers
{
    public class HomeController : BaseController
    {
        public override string ViewPath => "Home";

        public override void FillModel()
        {
            Model = new Home();
            Model.DatePublished = CurrentPage.UpdateDate;
            Model.Title = CurrentPage.GetPropertyValue<string>("Title");
        }
    }
}