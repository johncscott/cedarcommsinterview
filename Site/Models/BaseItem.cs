﻿using Site.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Models
{
    public class BaseItem : IBaseItem
    {
        public string Title { get; set; }

        public DateTime DatePublished { get; set; }
    }
}