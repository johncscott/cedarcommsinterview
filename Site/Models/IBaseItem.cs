﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Site.Models
{
    public interface IBaseItem
    {
        string Title { get; set; }
        DateTime DatePublished { get; set; }
    }
}