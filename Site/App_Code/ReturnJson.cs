﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using Umbraco.Web.WebApi;

namespace Site.App_Code
{
    [Route("api/returnJson")]
    public class ReturnJsonController : UmbracoApiController
    {
        [HttpGet]
        public string Test()
        {
            return "ws";
        }
        [HttpGet]
        public articleVM articleVM()
        {

            var rootNodes = Umbraco.TypedContentAtRoot();
            var articleNode = rootNodes.FirstOrDefault(x => x.DocumentTypeAlias == "interviewArticle");

            return new articleVM
            {
                title = articleNode.GetProperty("title").Value.ToString(),
                content = articleNode.GetProperty("content").Value.ToString()
            };
        }

    }
}